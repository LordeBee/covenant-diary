import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'
import "materialize-css";
import { MaterializeModule } from 'angular2-materialize';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { NgxPaginationModule } from 'ngx-pagination';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { AuthGuard } from './auth/auth.guard';
import { ApiServiceService } from './service/api-service.service';
import { AddCovenantComponent } from './add-covenant/add-covenant.component';
import { CovenantViewComponent } from './covenant-view/covenant-view.component';
import { AssignCovenantComponent } from './assign-covenant/assign-covenant.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    AddCovenantComponent,
    CovenantViewComponent,
    AssignCovenantComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MaterializeModule,
    AppRoutingModule,
    HttpClientModule,
    Ng2SearchPipeModule,
    Ng2OrderModule,
    NgxPaginationModule
  ],
  providers: [AuthGuard,ApiServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
