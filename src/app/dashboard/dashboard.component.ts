import { Component, OnInit, EventEmitter } from '@angular/core';
import { ApiServiceService } from '../service/api-service.service';
import { StorageServiceService } from '../service/storage-service.service';
import { Router } from '@angular/router';
import { toast } from 'angular2-materialize';
import { hostHeaderInfo } from '../models/hostHeaderInfo';
import { statusModel } from '../models/status';
import { idObj } from '../models/idObj';
import { tick } from '@angular/core/testing';

declare var jQuery:any;


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  username:any;
  public statusModel :statusModel;
  suite=false;
  resp:any;
  Loading = true;
  isData = false;
  errorMessage:any;
  covenants:any;
  covStatus:any;
  commentsDiv=false;
  response:any;
  public idObj: idObj;
  checkResp:any;
  checklist:any;
  statusErrorMessage:any;
  loadingStatus=true;
  isStatus=false;
  modalLoading=false;
  formInfo:any;
  formData:any;
  loaderd=false;
  resp2:any;

  


  constructor(private api_service: ApiServiceService, private router: Router, private storage_service: StorageServiceService) { 
    this.statusModel = new statusModel();
    this.covStatus = '';
    this.idObj = new idObj();

    if(!this.formData){
      this.formData={
        hostHeaderInfo: {
          requestId: "",
          ipAddress: "",
          sourceChannelId: ""
      },
      status:"removed",
      appid:""
      }
    }


  }

  ngOnInit() {

    this.get_Profile();
    this.statusModel.status = "ACTIVE";
    this.get_Suite();
  
  }
  logout(){
    var key ="covenantUserObj";
    this.storage_service.clearInfo(key);
    this.storage_service.clearInfo('suite');
    this.storage_service.clearInfo('appsData');
    window.location.href = 'https://ghuatgodigisrv1.gh.sbicdirectory.com:8005/moby/login';
  }



apps(){
  var key ="covenantUserObj";
    this.storage_service.clearInfo(key);
    window.location.href = 'https://ghuatgodigisrv1.gh.sbicdirectory.com:8005/moby/apps';
}

  newRecord(){
    this.router.navigate(['/add-covenant']);
  }

  get_Profile(){
    var key = 'covenantUserObj';
    var userInfo = JSON.parse(this.storage_service.getInfo(key));
    if(userInfo != null){
      this.username = userInfo.username;
    }
  }

  get_Suite(){
    var key = 'suite';
    var suite = this.storage_service.getInfo(key);
    if(suite){
      this.suite = true;
      this.statusModel.suite = suite;
      this.getData();
      
    }

    else{
      this.suite = false;
    }
  }

  suiteCheck():boolean{
    var resp = false;
    var key = 'suite';
    var suite = this.storage_service.getInfo(key);
    if(suite){
      resp = true;
    }

    else{
      resp =  false;
    }


    return resp;
  }

  toggleView(value: string){

    this.getData()
    
  }

  suiteInfo(){
    if(this.statusModel.suite!=""){
      this.suite = true;
      var check = this.suiteCheck();

      if(check==false){
        this.storage_service.saveInfo('suite', this.statusModel.suite);
      }
      else{
        this.storage_service.clearInfo('suite');
        this.storage_service.saveInfo('suite', this.statusModel.suite);
      }
      this.getData()
      
    }
  }

  getData(){
    this.Loading = true;
    var data = JSON.stringify(this.statusModel);
    this.api_service.get_covenants(data).subscribe(res=>{
    this.resp = res;
    if (this.resp.hostHeaderInfo.responseCode == '000') {
      this.covenants =  this.resp.covInfo;
      console.log(this.covenants);
      this.Loading = false;
      this.isData = true;
    }
    else{
      this.Loading = false;
      this.isData = false;
      this.errorMessage = "Sorry there are no records";    

    }

   },error=>{
    this.Loading = false;
    this.isData = false;
    this.errorMessage = "Error loading records";
    
  });

  }

  errorToast(){
    toast("Ooops! Please try again later",3000);
  }

  gotoView(){
    let elementId: string = (event.target as Element).id;
    this.router.navigate(['/covenant-view', elementId]);
  }

 

 


  watchlist(){
    this.router.navigate(['/watchlist']);
  }

  goto_Status(event: Event){
    let elementId: string = (event.target as Element).id;
    this.idObj.id = elementId;
    jQuery('.modal').modal(
      {
          dismissible: false,
          opacity: .5,
          inDuration: 300,
          outDuration: 200,
          startingTop: '1%',
          endingTop: '10%'
      }
  );

  jQuery('.statusModal').modal('open');
  event.preventDefault();
  this.watchlistInfo();
    
    
  }

  watchlistInfo(){
    this.loadingStatus = true;
    var info = JSON.stringify(this.idObj);
    this.api_service.get_checklist(info).subscribe(res=>{
      this.checkResp = res;  
      if(this.checkResp.hostHeaderInfo.responseCode == "000"){	
        this.checkStatus();
      }
      else{
        this.loadingStatus = false;
          this.isStatus = false;
          this.statusErrorMessage = "No comments found";
      }
    },error=>{
      this.loadingStatus = false;
          this.isStatus = false;
          this.statusErrorMessage = "Error loading records";;
    });
  }

  checkStatus(){
    
    if(this.checkResp.trackerInfo){
      var count = this.checkResp.trackerInfo.length;
      for (var i = 0; i < count; i++) {   

        if(this.checkResp.trackerInfo[i].status =="NOT MET"){
          this.checklist = this.checkResp.trackerInfo;
          this.loadingStatus = false;
          this.isStatus = true;
          console.log(this.checkResp.trackerInfo[i]);
          break;

        }

        else{
          this.loadingStatus = false;
          this.isStatus = false;
          this.statusErrorMessage = "No comments found";
        }
      }
    }

    
  }

  reAssign(){
    this.router.navigate(['/assign-covenant'])
  }


  disable(event: Event) {
    let elementId: string = (event.target as Element).id;
    this.formData.appid = elementId;


    jQuery('.modal').modal(
      {
        dismissible: true,
        opacity: .5,
        inDuration: 300,
        outDuration: 200,
        startingTop: '1%',
        endingTop: '10%',
      }
    );
    jQuery('.confirmDeactivateModal').modal('open');
    
    
  }


    disableAction(){
      this.loaderd = true;
      var data = JSON.stringify(this.formData);
      this.api_service.remove_covenant(data).subscribe(res => {
        this.resp2 = res;
        if(this.resp2.hostHeaderInfo.responseCode == "000"){
            toast('Record removed successfully',3000);
            this.getData();
            event.preventDefault();
            jQuery('.confirmDeactivateModal').modal('close');
            this.loaderd = false;
        }else{
          toast(this.resp2.hostHeaderInfo.responseMessage.toLowerCase(), 3000);
          this.loaderd = false;
          }
        }, error => {
            toast('Oops. Please try again later', 3000);
            this.loaderd = false;
      });
    }

    changeData(value: string){
      this.getData();
    }


  


}
