import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignCovenantComponent } from './assign-covenant.component';

describe('AssignCovenantComponent', () => {
  let component: AssignCovenantComponent;
  let fixture: ComponentFixture<AssignCovenantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignCovenantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignCovenantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
