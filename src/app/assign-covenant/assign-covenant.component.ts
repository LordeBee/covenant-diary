import { Component, OnInit, EventEmitter } from '@angular/core';
import { ApiServiceService } from '../service/api-service.service';
import { StorageServiceService } from '../service/storage-service.service';
import { Router,ActivatedRoute } from '@angular/router';
import { assignModel } from '../models/assignModel';
import { hostHeaderInfo } from '../models/hostHeaderInfo';
import { toast } from 'angular2-materialize';

declare var jQuery:any;

@Component({
  selector: 'app-assign-covenant',
  templateUrl: './assign-covenant.component.html',
  styleUrls: ['./assign-covenant.component.css']
})
export class AssignCovenantComponent implements OnInit {

  username:any;
  public assignModel : assignModel;
  public hostHeaderInfo : hostHeaderInfo;
  loaderd=false;
  subresp:any;

  constructor(private api_service: ApiServiceService, private router: Router,private route: ActivatedRoute, private storage_service: StorageServiceService) {
    
    this.assignModel = new assignModel();
    this.hostHeaderInfo = new hostHeaderInfo();
    this.assignModel.hostHeaderInfo = this.hostHeaderInfo;

  }

  ngOnInit() {
    this.get_Profile();
  }

  logout(){
    var key ="covenantUserObj";
    this.storage_service.clearInfo(key);
    this.storage_service.clearInfo('suite');
    this.storage_service.clearInfo('appsData');
    window.location.href = 'https://ghuatgodigisrv1.gh.sbicdirectory.com:8005/moby/login';
  }



apps(){
  var key ="covenantUserObj";
    this.storage_service.clearInfo(key);
    window.location.href = 'https://ghuatgodigisrv1.gh.sbicdirectory.com:8005/moby/apps';
}



  dashboard(){
    this.router.navigate(['/dashboard']);
  }
  get_Profile(){
    var key = 'covenantUserObj';
    var userInfo = JSON.parse(this.storage_service.getInfo(key));
    if(userInfo != null){
      this.username = userInfo.username;
      

    }
  }

  submitModal(){
    if(this.assignModel.oldRmSapid=='' || this.assignModel.newRmSapid==''){
      toast("Fill all fields",3000);
    }
    else if((this.assignModel.oldRmSapid.length!=7) || (this.assignModel.newRmSapid.length!=7)){
      toast("Enter a valid SAP number",3000)
    }
    else{
      jQuery('.modal').modal(
        {
          dismissible: false,
          opacity: .5,
          inDuration: 300,
          outDuration: 200,
          startingTop: '1%',
          endingTop: '10%',
        }
      );
      jQuery('.confirmActivateModal').modal('open');
    }
   

  }

  activateAction(){
    this.loaderd = true;

    var data = JSON.stringify(this.assignModel);
    this.api_service.update_rm(data).subscribe(res=>{

      this.subresp = res;
      if(this.subresp.hostHeaderInfo.responseCode == "000"){	
        toast("Submitted successfully",3000);
        this.router.navigate(['/dashboard']);
        jQuery('.confirmActivateModal').modal('close');
    
      }
      else{
        this.loaderd = false;
        toast(this.subresp.hostHeaderInfo.responseMessage ,3000);
      }
    
    },error=>{
      this.loaderd = false;
      toast("Ooops, Please try again later",3000);
    });

   
  }

}
