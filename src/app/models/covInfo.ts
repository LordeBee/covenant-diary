export class covInfo{
    id: string = '';
    nameOfCounterparty: string = '';
    rm: string = '';
    rmCode: string = '';
    frequency: string = '';
    monRequirement: string = '';
    suite: string = '';
    nextdue: string = '';
    cif: string='';
    covenant: string="";
    timeAllowed: string='';
    followUpDate: string='';
    createdBy: string='';
    status: string = '';
    modifiedBy: string='';
    notify: string='';
    skipToNextQuarter: string='';
    reportedOnWatchlist: string='';
}