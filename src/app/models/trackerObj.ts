import {hostHeaderInfo } from './hostHeaderInfo';
import { trackerInfo} from './trackerInfo';

export class trackerObj{
    hostHeaderInfo : hostHeaderInfo;
    trackerInfo: trackerInfo;
}