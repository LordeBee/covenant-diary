import { Component, OnInit, EventEmitter } from '@angular/core';
import { ApiServiceService } from '../service/api-service.service';
import { StorageServiceService } from '../service/storage-service.service';
import { Router } from '@angular/router';
import { MaterializeAction } from 'angular2-materialize';
import { toast } from 'angular2-materialize';
import { hostHeaderInfo } from '../models/hostHeaderInfo';
import { cifModel } from '../models/cifModel';
import { covInfo } from '../models/covInfo';
import { covInfoObj } from '../models/covInfoObj';
import { statusModel } from '../models/status';
import {DatePipe} from '@angular/common';



declare var $:any;

@Component({
  selector: 'app-add-covenant',
  templateUrl: './add-covenant.component.html',
  providers:[DatePipe],
  styleUrls: ['./add-covenant.component.css']
})
export class AddCovenantComponent implements OnInit {

  username:any;
  navigation:any;
  public cifModel : cifModel;
  public covInfoObj : covInfoObj;
  public hostHeaderInfo : hostHeaderInfo;
  public covInfo : covInfo;
  resp:any;
  loading=false;
  accVerify=false;
  response:any;
  public statusModel :statusModel;
  subresp:any;
  subloading=false;

  autocompleteInit: any;
  requirements:any;
  optionsData: any;

  quaterDiv=false;
  dateDiv=false;
  customerValue:any;
  rmValue:any;

  expiryDate:any;
  skipLabel:any;

  constructor(private api_service: ApiServiceService, private router: Router, private storage_service: StorageServiceService,private datePipe: DatePipe) {
    this.cifModel = new cifModel();
    this.covInfoObj = new covInfoObj();
    this.covInfoObj.hostHeaderInfo = new hostHeaderInfo();
    this.covInfoObj.covInfo = new covInfo();
    this.statusModel = new statusModel();
    this.customerValue = '';
    this.rmValue = '';
   }

  ngOnInit() {
    this.get_Profile();
    this.navigation=1;
    this.getMonitoringReq();
    this.optionsData = {};

    // this.getExpiryDate();
    

    
  }


  checkRm(){
    $( "#rm" ).prop( "checked", true );
  }

  logout(){
    var key ="covenantUserObj";
    this.storage_service.clearInfo(key);
    this.storage_service.clearInfo('suite');
    this.storage_service.clearInfo('appsData');
    window.location.href = 'https://ghuatgodigisrv1.gh.sbicdirectory.com:8005/moby/login';
  }



apps(){
  var key ="covenantUserObj";
    this.storage_service.clearInfo(key);
    window.location.href = 'https://ghuatgodigisrv1.gh.sbicdirectory.com:8005/moby/apps';
}




  dashboard(){
    this.router.navigate(['/dashboard']);
  }
  get_Profile(){
    var key = 'covenantUserObj';
    var userInfo = JSON.parse(this.storage_service.getInfo(key));
    if(userInfo != null){
      this.username = userInfo.username;
      this.covInfoObj.covInfo.createdBy = userInfo.userID;
    }
  }
  next(){
    if(this.navigation != 2)this.navigation++ 
    $("#prog").css("width", "100%");
  }
  prev(){
    if(this.navigation != 1)this.navigation--
    $("#prog").css("width", "50%");
  }

  accInfo(){
 
      if(this.cifModel.cif===''){
        toast("Please enter a CIF",3000);
      }

    else{
        this.loading = true;
        var info = JSON.stringify(this.cifModel);
        this.api_service.accData(info).subscribe(res=>{
          this.resp = res;
          if(this.resp.hostHeaderInfo.responseCode == "000"){	
            this.loading = false;
            this.accVerify = true;
            this.covInfoObj.covInfo.nameOfCounterparty = this.resp.accountsInfo[0].accountName;
            this.covInfoObj.covInfo.rm = this.resp.relationManagerInfo.rmName;
            this.covInfoObj.covInfo.rmCode = this.resp.relationManagerInfo.rmCode;
  

          }
          else{
            this.loading = false;
            toast(this.resp.hostHeaderInfo.responseMessage ,3000);
          }
        },error=>{
          this.loading = false;
          toast("Ooops please try again" ,3000);
        }); 
    }
  }  
  
  submit(){

    if((this.covInfoObj.covInfo.suite=='')||(this.covInfoObj.covInfo.frequency=='')
    ||(this.covInfoObj.covInfo.covenant=='')||(this.covInfoObj.covInfo.monRequirement=='')
    ||(this.covInfoObj.covInfo.timeAllowed=='')||(this.checkNotification()==false)||(this.checkFrequecy()==false)){
      toast("Fill all required fields",3000);
    }
    else{
      this.subloading = true;
      this.covInfoObj.covInfo.reportedOnWatchlist = "N";
      this.covInfoObj.covInfo.cif = this.cifModel.cif;
      if((this.customerValue!='') && (this.rmValue!='')){
        this.covInfoObj.covInfo.notify = "RM/CUSTOMER"
      }else if((this.customerValue=='') && (this.rmValue!='')){
        this.covInfoObj.covInfo.notify = "RM";
      }else if((this.customerValue!='') && (this.rmValue=='')){
        this.covInfoObj.covInfo.notify = "RM/CUSTOMER";
      }
      this.covInfoObj.covInfo.skipToNextQuarter = this.statusModel.status;
      if((this.covInfoObj.covInfo.frequency=="Monthly") || (this.covInfoObj.covInfo.frequency=="One-Off")){
        this.dateDiv = true;
      }
      var data = JSON.stringify(this.covInfoObj);

      this.api_service.add_covenant(data).subscribe(res=>{
        this.subresp = res;

        if(this.subresp.hostHeaderInfo.responseCode == "000"){	
          toast("Submitted successfully",3000);
          this.router.navigate(['/dashboard']);
          this.subloading = false;
      
        }
        else{
          this.subloading = false;
          toast(this.subresp.hostHeaderInfo.responseMessage ,3000);
        }
      
      },error=>{
        this.subloading = false;
        toast("Ooops, Please try again later",3000);
      });
    }

  }
  watchlist(){
    this.router.navigate(['/watchlist']);
  }
  
  getMonitoringReq(){
    this.api_service.monitoring_req().subscribe(res=>{
      this.response = res;
      if (this.response.hostHeaderInfo.responseCode == '000') {
        this.requirements = this.response.monitoringInfo;
        console.log(this.requirements);
        // this.createReqObjs();
    }
    },error=>{
      toast("Ooops, Couldnt fetch monitoring requiremnt at this time",3000);
    });
  }

  createReqObjs(){
    for (var i = 0; i < this.requirements.length; i++) {
      var obj = this.requirements[i];
      this.optionsData[obj.name] = null;
      if (i == this.requirements.length - 1) {
          this.autocompleteInit = {
              'data': this.optionsData,
              onAutocomplete: (val) => {
                  this.covInfoObj.covInfo.monRequirement = val;
              },
          };
      }
    }
  }

  frequency(value: string){

    if((value=="Monthly") || (value=="One-Off")){
      $(".freq").prop('disabled', false);
      this.quaterDiv = false;
      this.dateDiv = true;
      this.getExpiryDate()
    }
    else if((value=="Quarterly")){
      this.skipLabel = 'Skip This Quater?';
      this.quaterDiv = true;
      $(".freq").prop('disabled', true);
      this.dateDiv = false;
      this.getExpiryDate()

    }

    else if((value=="Half-Yearly")){
      this.skipLabel = 'Skip This Half?';
      this.quaterDiv = true;
      $(".freq").prop('disabled', true);
      this.dateDiv = false;
      this.getExpiryDate()
    }
    else{
      this.covInfoObj.covInfo.nextdue = '';
      $(".freq").prop('disabled', true);
      this.quaterDiv = false;
      this.dateDiv = false;
      this.getExpiryDate()
    }
    
  }

  checkNotification():boolean{
    var resp;
    resp = false;

    if(this.rmValue!=''){
      resp =  true;
    }
    else if(this.customerValue!=''){
      resp =  true;
    }
    else if((this.rmValue!='')&&(this.customerValue!='')){
      resp =  true;
    }
    else{
      resp = false;
    }
    return resp;
  }


  checkFrequecy():boolean{
    var resp;
    resp = false;

    if(((this.covInfoObj.covInfo.frequency=="Monthly") || (this.covInfoObj.covInfo.frequency=="One-Off"))&& (this.covInfoObj.covInfo.nextdue!='')){
      resp =  true;
    }
    else if((this.covInfoObj.covInfo.frequency=="Quarterly")&&(this.statusModel.status!='')){
      resp =  true;
    }

    else if((this.covInfoObj.covInfo.frequency=="Half-Yearly")&&(this.statusModel.status!='')){
      resp =  true;
    }
    else if((this.covInfoObj.covInfo.frequency=="Yearly")||(this.covInfoObj.covInfo.frequency=="Weekly")){
      resp =  true;
    }
    else{
      resp = false;
    }
    return resp;
  }

  getExpiryDate(){

    if((this.covInfoObj.covInfo.frequency=='One-Off')&&(this.covInfoObj.covInfo.nextdue!='')){

      if(this.covInfoObj.covInfo.timeAllowed!=''){
        var a =  parseInt(this.covInfoObj.covInfo.timeAllowed);
      }
      else{
        var a = 0;
      }
        
        var toUTC = new Date(this.covInfoObj.covInfo.nextdue);
        toUTC.setDate(toUTC.getDate() + a);
        if(toUTC){
          this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
        }
        

    }

    else if((this.covInfoObj.covInfo.frequency=='Weekly')){
      if(this.covInfoObj.covInfo.timeAllowed!=''){
        var a =  parseInt(this.covInfoObj.covInfo.timeAllowed);
      }
      else{
        var a = 0;
      }
      var b = 7 + a;
      var toUTC = new Date();
      toUTC.setDate(toUTC.getDate() + b);
      if(toUTC){
        this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
      }
    }

    else if((this.covInfoObj.covInfo.frequency=='Monthly')&&(this.covInfoObj.covInfo.nextdue!='')){
      if(this.covInfoObj.covInfo.timeAllowed!=''){
        var a =  parseInt(this.covInfoObj.covInfo.timeAllowed);
      }
      else{
        var a = 0;
      }
      var toUTC = new Date(this.covInfoObj.covInfo.nextdue);
      toUTC.setMonth(toUTC.getMonth() + 1);
      toUTC.setDate(toUTC.getDate() + a);
      if(toUTC){
        this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
        console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
      }
    }

    else if((this.covInfoObj.covInfo.frequency=='Quarterly')&&(this.statusModel.status!='')){
      var year = new Date().getFullYear();
      var newYear = new Date().getFullYear() + 1;

      if(this.covInfoObj.covInfo.timeAllowed!=''){
        var a =  parseInt(this.covInfoObj.covInfo.timeAllowed);
      }
      else{
        var a = 0;
      }

      var today = new Date();
      var month = today.getMonth() + 1;
      var cur = (Math.ceil(month / 3));
      if((cur == 4)&&(this.statusModel.status=="N")){

        var c = "31-Dec-"+year;
        var toUTC = new Date(c);
        toUTC.setDate(toUTC.getDate() + a);
        if(toUTC){
          this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
          console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
        }

      }
      else if((cur == 4)&&(this.statusModel.status=="Y")){
        var c = "31-Mar-"+newYear;
        var toUTC = new Date(c);
        toUTC.setDate(toUTC.getDate() + a);
        if(toUTC){
          this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
          console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
        }

      }
      else if((cur == 3)&&(this.statusModel.status=="N")){
        var c = "30-Sep-"+year;
        var toUTC = new Date(c);
        toUTC.setDate(toUTC.getDate() + a);
        if(toUTC){
          this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
          console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
        }

      }

      else if((cur == 3)&&(this.statusModel.status=="Y")){
        var c = "31-Dec-"+year;
        var toUTC = new Date(c);
        toUTC.setDate(toUTC.getDate() + a);
        if(toUTC){
          this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
          console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
        }

      }

      else if((cur == 2)&&(this.statusModel.status=="N")){
        var c = "30-Jun-"+year;
        var toUTC = new Date(c);
        toUTC.setDate(toUTC.getDate() + a);
        if(toUTC){
          this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
          console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
        }

      }

      else if((cur == 2)&&(this.statusModel.status=="Y")){
        var c = "30-Sep-"+year;
        var toUTC = new Date(c);
        toUTC.setDate(toUTC.getDate() + a);
        if(toUTC){
          this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
          console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
        }

      }

      else if((cur == 1)&&(this.statusModel.status=="N")){
        var c = "31-Mar-"+year;
        var toUTC = new Date(c);
        toUTC.setDate(toUTC.getDate() + a);
        if(toUTC){
          this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
          console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
        }

      }

      else if((cur == 1)&&(this.statusModel.status=="Y")){
        var c = "30-Jun-"+year;
        var toUTC = new Date(c);
        toUTC.setDate(toUTC.getDate() + a);
        if(toUTC){
          this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
          console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
        }

      }
      console.log(cur)

    }

    else if((this.covInfoObj.covInfo.frequency=='Half-Yearly')&&(this.statusModel.status!='')){
      var year = new Date().getFullYear();
      var newYear = new Date().getFullYear() + 1;

      if(this.covInfoObj.covInfo.timeAllowed!=''){
        var a =  parseInt(this.covInfoObj.covInfo.timeAllowed);
      }
      else{
        var a = 0;
      }

      var today = new Date();
      var month = today.getMonth() + 1;
      var cur = (Math.ceil(month / 6));
      if((cur == 2)&&(this.statusModel.status=="N")){

        var c = "31-Dec-"+year;
        var toUTC = new Date(c);
        toUTC.setDate(toUTC.getDate() + a);
        if(toUTC){
          this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
          console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
        }

      }

      else if((cur == 2)&&(this.statusModel.status=="Y")){

        var c = "30-Jun-"+newYear;
        var toUTC = new Date(c);
        toUTC.setDate(toUTC.getDate() + a);
        if(toUTC){
          this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
          console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
        }

      }

      else if((cur == 1)&&(this.statusModel.status=="N")){

        var c = "30-Jun-"+year;
        var toUTC = new Date(c);
        toUTC.setDate(toUTC.getDate() + a);
        if(toUTC){
          this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
          console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
        }

      }

      else if((cur == 1)&&(this.statusModel.status=="Y")){

        var c = "31-Dec-"+year;
        var toUTC = new Date(c);
        toUTC.setDate(toUTC.getDate() + a);
        if(toUTC){
          this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
          console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
        }

      }

    }

    else if((this.covInfoObj.covInfo.frequency=='Yearly')){
      var year = new Date().getFullYear();

      if(this.covInfoObj.covInfo.timeAllowed!=''){
        var a =  parseInt(this.covInfoObj.covInfo.timeAllowed);
      }
      else{
        var a = 0;
      }

      var c = "31-Dec-"+year;

      var toUTC = new Date(c);
      toUTC.setDate(toUTC.getDate() + a);
      if(toUTC){
        this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
      }
    }
  

  }

  statusChange(value: string){
    this.getExpiryDate();
  }


}
