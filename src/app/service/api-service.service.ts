import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

// private baseUrl = "/api/";
 private baseUrl = "https://ghuatgodigisrv1.gh.sbicdirectory.com:8005/covenant/api/";

  private loginURL = this.baseUrl + "user_login";
  private accCifURL = this.baseUrl + "account_info";
  private accNoUrl = this.baseUrl + "account_info_acc";
  private monitorReqUrl = this.baseUrl + "monitoring";
  private addCovenantUrl = this.baseUrl + "addCovInfo";
  private dashboardUrl= this.baseUrl + "covenants"; 
  private updateStatusUrl= this.baseUrl + "updatetracker"; 
  private getChecklistUrl= this.baseUrl + "tracker";
  private updateCovenantUrl = this.baseUrl + "updateCovInfo";
  private getCovenantByIdUrl = this.baseUrl + "covenantsbyid"; 
  private updateRmInfoUrl = this.baseUrl + "updateRmInfo";
  private updateRecordStatusUrl = this.baseUrl + "updatestatus";

  
  private httpHeaders = new HttpHeaders()
    .set('Content-Type', 'application/json')
    .set('Access-Control-Allow-Origin', '*')
    .set("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT")
    .set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
    .set("sourceCode", "Covenant-Diary")
    .set("countryCode", "GH")

  private options = {
    headers: this.httpHeaders
  };


  constructor(private _http:HttpClient) { }

  authUser(data){
    return this._http.post(this.loginURL, data, this.options);
  }

  accData(data){
    return this._http.post(this.accCifURL, data, this.options);
  }

  accDataNo(data){
    return this._http.post(this.accNoUrl, data, this.options);
  }

  monitoring_req(){
    return this._http.post(this.monitorReqUrl, this.options);
  }

  add_covenant(data){
    return this._http.post(this.addCovenantUrl, data, this.options);
  }

  get_covenants(data){
    return this._http.post(this.dashboardUrl,data, this.options);
  }

  update_tracker(data){
    return this._http.patch(this.updateStatusUrl, data, this.options);
  }

  get_covenant(data){
    return this._http.post(this.getCovenantByIdUrl,data, this.options);
  }
  get_checklist(data){
    return this._http.post(this.getChecklistUrl,data, this.options);
  }

  update_covenant(data){
    return this._http.patch(this.updateCovenantUrl, data, this.options);
  }

  update_rm(data){
    return this._http.patch(this.updateRmInfoUrl,data,this.options);
  }

  remove_covenant(data){
    return this._http.patch(this.updateRecordStatusUrl, data, this.options);
  }


}
