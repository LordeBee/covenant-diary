import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { toast } from 'angular2-materialize';
import { ApiServiceService } from '../service/api-service.service';
import { StorageServiceService } from '../service/storage-service.service';
import { loginModel } from '../models/loginModel';
import { hostHeaderInfo } from '../models/hostHeaderInfo';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  resp: any;
  // loginModel:any;
  userObj:any;
  loading=false;

  public loginModel : loginModel;
  public hostHeaderInfo : hostHeaderInfo;

  constructor(private api_service: ApiServiceService, private router: Router, private storage_service: StorageServiceService) { 
    this.loginModel = new loginModel();
    this.hostHeaderInfo = new hostHeaderInfo();
    this.loginModel.hostHeaderInfo = this.hostHeaderInfo;
    
    if (!this.userObj) {this.userObj = {userID: '',username: '',application: ''}}
  }

  ngOnInit() {

  }

  login(){
    if(this.loginModel.username === ''){
      this.showToast('Please enter your SAP number');
    }else if (this.loginModel.password === ''){
      this.showToast('Please enter your password')
    }else{
      this.loading = true;
      this.loginModel.username = this.loginModel.username.toUpperCase();
      var data = JSON.stringify(this.loginModel);
      // alert(data);
      this.api_service.authUser(data).subscribe(res => {
        this.resp = res;
        this.loading = false;
        if(this.resp.hostHeaderInfo.responseCode == "000"){
          var auth = this. checkApp();
          if(auth==true){
            this.userObj.userID = this.loginModel.username;
            this.userObj.username = this.resp.staffInfo.firstName;
            this.userObj.application = "COVENANT DIARY";
            this.storage_service.saveInfo('covenantUserObj', JSON.stringify(this.userObj));
            this.router.navigate(['/dashboard']);
          }

          else{
            toast("Please contact system admin for access",3000);
          }

          
        }else{
          toast(this.resp.hostHeaderInfo.responseMessage.toLowerCase(), 3000);
        }
    }, error => {
        toast('Oops. Please try again later', 3000);
        this.loading = false;
    });
    }
  }

  showToast(message: string) {
    toast(message, 3000);
  }

  checkApp():boolean{
    var state = true;
  
    if(this.resp.role){
      var count = this.resp.role.length;

      for (var i = 0; i < count; i++) {

        if(this.resp.role[i].app==="COVENANT DIARY"){ 
          state = true;
          break;
        }
  
        else{
          state = false;
          
        }
  
      }

    }

    else if(!this.resp.role){
      state = false;
    }
   
    return state;  

  }

}
