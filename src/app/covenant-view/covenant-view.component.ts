import { Component, OnInit, EventEmitter } from '@angular/core';
import { ApiServiceService } from '../service/api-service.service';
import { StorageServiceService } from '../service/storage-service.service';
import { Router,ActivatedRoute } from '@angular/router';
import { MaterializeAction } from 'angular2-materialize';
import { toast } from 'angular2-materialize';
import { hostHeaderInfo } from '../models/hostHeaderInfo';
import { idObj } from '../models/idObj';
import { covInfo } from '../models/covInfo';
import { covInfoObj } from '../models/covInfoObj';
import { cifModel } from '../models/cifModel';
import { trackerObj } from '../models/trackerObj';
import { trackerInfo } from '../models/trackerInfo';
import { statusModel } from '../models/status';
import {DatePipe} from '@angular/common';

declare var $:any;
declare var jQuery:any;

@Component({
  selector: 'app-covenant-view',
  templateUrl: './covenant-view.component.html',
  providers:[DatePipe],
  styleUrls: ['./covenant-view.component.css']
})
export class CovenantViewComponent implements OnInit {

  username:any;
  sub:any;
  public idObj: idObj;
  resp:any;
  appId:any;
  public covInfoObj : covInfoObj;
  public hostHeaderInfo : hostHeaderInfo;
  public covInfo : covInfo;
  public cifModel : cifModel;
  navigation:any;
  public trackerObj:  trackerObj;

  autocompleteInit: any;
  requirements:any;
  optionsData: any;
  response:any;
  loading:any;
  accVerify=false;
  checkResp:any;
  checklist:any;
  covStatus:any;

  check:any;
  result=false;
  subloading=false;
  subresp:any;
  ic:any;

  modalLoading = false
  tracks=false;
  Loading = false;
  

  quaterDiv = false;
  dateDiv = false;
  public statusModel :statusModel;

  customerValue:any;
  rmValue:any;

  expiryDate:any;
  skipLabel:any;

  loaderd=false;

  constructor(private api_service: ApiServiceService, private router: Router,private route: ActivatedRoute, private storage_service: StorageServiceService,private datePipe: DatePipe) { 
    this.idObj = new idObj();
    this.covInfoObj = new covInfoObj();
    this.covInfoObj.hostHeaderInfo = new hostHeaderInfo();
    this.covInfoObj.covInfo = new covInfo();
    this.cifModel = new cifModel();
    this.trackerObj = new trackerObj();
    this.trackerObj.hostHeaderInfo = new hostHeaderInfo();
    this.trackerObj.trackerInfo = new trackerInfo();
    this.covStatus = '';
    this.result = true;
    this.statusModel = new statusModel();
    this.customerValue = '';
    this.rmValue = '';

    
  }

  ngOnInit() {
    this.get_Profile();
    this.navigation=1;
    this.getMonitoringReq();
    this.optionsData = {};

    this.sub = this.route.params.subscribe(params => {
      this.idObj.id= params['uuid'];
      this.getData();
      this.getChecklist();
    });
  }

  logout(){
    var key ="covenantUserObj";
    this.storage_service.clearInfo(key);
    this.storage_service.clearInfo('suite');
    this.storage_service.clearInfo('appsData');
    window.location.href = 'https://ghuatgodigisrv1.gh.sbicdirectory.com:8005/moby/login';
  }



apps(){
  var key ="covenantUserObj";
    this.storage_service.clearInfo(key);
    window.location.href = 'https://ghuatgodigisrv1.gh.sbicdirectory.com:8005/moby/apps';
}

  newRecord(){
    this.router.navigate(['/add-covenant']);
  }

  dashboard(){
    this.router.navigate(['/dashboard']);
  }
  get_Profile(){
    var key = 'covenantUserObj';
    var userInfo = JSON.parse(this.storage_service.getInfo(key));
    if(userInfo != null){
      this.username = userInfo.username;
      this.covInfoObj.covInfo.modifiedBy = userInfo.userID;
    }
  }

  getData(){

    var data = JSON.stringify(this.idObj);
    this.api_service.get_covenant(data).subscribe(res=>{

      this.resp = res;
      console.log(this.resp)

      if(this.resp.hostHeaderInfo.responseCode=="000"){
        this.covInfoObj.covInfo.id = this.resp.covInfo[0].id;
        this.covInfoObj.covInfo.cif = this.resp.covInfo[0].cif;
        this.covInfoObj.covInfo.rm = this.resp.covInfo[0].rm;
        this.covInfoObj.covInfo.rmCode = this.resp.covInfo[0].rmCode;
        this.covInfoObj.covInfo.monRequirement = this.resp.covInfo[0].monRequirement;
        this.covInfoObj.covInfo.timeAllowed = this.resp.covInfo[0].timeAllowed;
        this.covInfoObj.covInfo.frequency = this.resp.covInfo[0].frequency;
        this.covInfoObj.covInfo.suite = this.resp.covInfo[0].suite;
        this.covInfoObj.covInfo.covenant = this.resp.covInfo[0].covenant;
        this.covInfoObj.covInfo.followUpDate = this.resp.covInfo[0].followUpDate;
        this.covInfoObj.covInfo.nameOfCounterparty = this.resp.covInfo[0].nameOfCounterparty;
        this.covInfoObj.covInfo.reportedOnWatchlist = this.resp.covInfo[0].reportedOnWatchlist;
        this.expiryDate = this.resp.covInfo[0].nextdue;
        this.statusModel.status = this.resp.covInfo[0].skipToNextQuarter;


        if((this.resp.covInfo[0].frequency=="MONTHLY") || (this.resp.covInfo[0].frequency=="ONE-OFF")){
          $(".freq").prop('disabled', false);
          this.quaterDiv = false;
          this.dateDiv = true;
          this.covInfoObj.covInfo.nextdue = this.resp.covInfo[0].untempDueDate;
        }
        else if(this.resp.covInfo[0].frequency=="QUARTERLY"){
          this.quaterDiv = true;
          $(".freq").prop('disabled', true);
          this.dateDiv = false;
          this.skipLabel = 'Skip This Quater?';
        }

        else if(this.resp.covInfo[0].frequency=="HALF-YEARLY"){
          this.skipLabel = 'Skip This Half?';
          this.quaterDiv = true;
          $(".freq").prop('disabled', true);
          this.dateDiv = false;
          this.getExpiryDate()
        }
        else{
          this.covInfoObj.covInfo.nextdue = '';
          $(".freq").prop('disabled', true);
          this.quaterDiv = false;
          this.dateDiv = false;
        }

        if(this.resp.covInfo[0].notify=="RM"){
          $( "#rm" ).prop( "checked", true );

        }
        else if((this.resp.covInfo[0].notify=="RM")||(this.resp.covInfo[0].notify=="CUSTOMER/RM")){
          $( "#rm" ).prop( "checked", true );
          $( "#customer" ).prop( "checked", true );
        }



      }

      else{
        toast(this.resp.hostHeaderInfo.responseMessage,3000);
      }

    }
    ,error=>{
      toast("Sorry couldn't submit at this time",3000);
    });

   
  }

  next(){
    if(this.navigation != 2)this.navigation++ 
    $("#prog").css("width", "100%");
  }
  prev(){
    
    if(this.navigation != 1)this.navigation--
    $("#prog").css("width", "50%");

    
  }

  accInfo(){
 
    if(this.covInfoObj.covInfo.cif===''){
      toast("Please enter a CIF",3000);
    }

    else{
        this.loading = true;
        this.cifModel.cif = this.covInfoObj.covInfo.cif;
        var info = JSON.stringify(this.cifModel);
        this.api_service.accData(info).subscribe(res=>{
          this.response = res;
          if(this.response.hostHeaderInfo.responseCode == "000"){	
            this.loading = false;
            this.accVerify = true;
            this.covInfoObj.covInfo.nameOfCounterparty = this.response.accountsInfo[0].accountName;
            this.covInfoObj.covInfo.rm = this.response.relationManagerInfo.rmName;
            this.covInfoObj.covInfo.rmCode = this.response.relationManagerInfo.rmCode;


          }
          else{
            this.loading = false;
            toast(this.response.hostHeaderInfo.responseMessage ,3000);
          }
        },error=>{
          this.loading = false;
          toast("Ooops please try again" ,3000);
        }); 
    }
}  

getMonitoringReq(){
  this.api_service.monitoring_req().subscribe(res=>{
    this.response = res;
    if (this.response.hostHeaderInfo.responseCode == '000') {
      this.requirements = this.response.monitoringInfo;
      this.createReqObjs();
  }
  },error=>{
    toast("Ooops, Couldnt fetch monitoring requiremnt at this time",3000);
  });
}

createReqObjs(){
  for (var i = 0; i < this.requirements.length; i++) {
    var obj = this.requirements[i];
    this.optionsData[obj.name] = null;
    if (i == this.requirements.length - 1) {
        this.autocompleteInit = {
            'data': this.optionsData,
            onAutocomplete: (val) => {
                this.covInfoObj.covInfo.monRequirement = val;
            },
        };
    }
  }
}

frequency(value: string){

  if((value=="MONTHLY") || (value=="ONE-OFF")){
    $(".freq").prop('disabled', false);
    this.quaterDiv = false;
    this.dateDiv = true;
    this.getExpiryDate()
  }
  else if((value=="QUARTERLY")){
    this.skipLabel = 'Skip This Quater?';
    this.quaterDiv = true;
    $(".freq").prop('disabled', true);
    this.dateDiv = false;
    this.getExpiryDate()

  }

  else if((value=="HALF-YEARLY")){
    this.skipLabel = 'Skip This Half?';
    this.quaterDiv = true;
    $(".freq").prop('disabled', true);
    this.dateDiv = false;
    this.getExpiryDate()
  }
  else{
    this.covInfoObj.covInfo.nextdue = '';
    $(".freq").prop('disabled', true);
    this.quaterDiv = false;
    this.dateDiv = false;
    this.getExpiryDate()
  }
  
}

getChecklist(){
  this.Loading = true;
  var data = JSON.stringify(this.idObj);
  this.api_service.get_checklist(data).subscribe(res=>{
    this.checkResp = res;
    this.Loading = false;

    if(this.checkResp.hostHeaderInfo.responseCode == "000"){	
      this.tracks = true;
      this.checklist = this.checkResp.trackerInfo;
    }
    else{
      this.tracks = false;
    }
  },error=>{
    this.tracks = false;
  });

}

submit(){
  if((this.covInfoObj.covInfo.cif !== this.resp.covInfo[0].cif)||
  (this.covInfoObj.covInfo.rm !== this.resp.covInfo[0].rm)||
  (this.covInfoObj.covInfo.rmCode !== this.resp.covInfo[0].rmCode)||
  (this.covInfoObj.covInfo.monRequirement !== this.resp.covInfo[0].monRequirement)||
  (this.covInfoObj.covInfo.timeAllowed !== this.resp.covInfo[0].timeAllowed)||
  (this.covInfoObj.covInfo.frequency !== this.resp.covInfo[0].frequency)||
  (this.covInfoObj.covInfo.suite !== this.resp.covInfo[0].suite)||
  (this.covInfoObj.covInfo.covenant !== this.resp.covInfo[0].covenant)||
  (this.covInfoObj.covInfo.nameOfCounterparty !== this.resp.covInfo[0].nameOfCounterparty)||
  (this.expiryDate !== this.resp.covInfo[0].nextdue)||
  (this.statusModel.status !== this.resp.covInfo[0].skipToNextQuarter)){

    jQuery('.modal').modal(
      {
        dismissible: false,
        opacity: .5,
        inDuration: 300,
        outDuration: 200,
        startingTop: '1%',
        endingTop: '10%',
      }
    );
    jQuery('.confirmActivateModal').modal('open');

  
  }
  else{
    toast("NO CHANGES MADE TO SUBMIT",3000);
  }
}


update(event: Event,checkDate){
  let elementId: string = (event.target as Element).id;
  this.trackerObj.trackerInfo.id = elementId;
  this.trackerObj.trackerInfo.checkDate = checkDate;
  jQuery('.modal').modal(
    {
        dismissible: true,
        opacity: .5,
        inDuration: 300,
        outDuration: 200,
        startingTop: '1%',
        endingTop: '10%'
    }
);

jQuery('.covenant-modal').modal('open');
event.preventDefault();


}

submitTracker(){

  if(this.covStatus==''){
    toast("Please select covenant status",3000);
  }
  else{
    if((this.covStatus=="NOT MET")&&(this.trackerObj.trackerInfo.amComment=="")){
      toast("Add AM comments",3000);
    }

    else{

      this.modalLoading = true;
      this.trackerObj.trackerInfo.status = this.covStatus;
      var data = JSON.stringify(this.trackerObj);
      console.log(data);
      this.api_service.update_tracker(data).subscribe(res=>{
        this.response = res;
        if(this.response.hostHeaderInfo.responseCode=="000"){
          toast("Updated successfully",3000);
          jQuery('.covenant-modal').modal('close');
          this.getChecklist();
          this.modalLoading = false;

        }

        else{
          toast(this.response.hostHeaderInfo.responseMessage,3000);
          this.modalLoading = false;
        }

      }
      ,error=>{
        toast("Sorry couldn't submit at this time",3000);
        this.modalLoading = false;
      });

    }
  }
  


}

checkRm(){
  $( "#rm" ).prop( "checked", true );
}

getExpiryDate(){

  if((this.covInfoObj.covInfo.frequency=='ONE-OFF')&&(this.covInfoObj.covInfo.nextdue!='')){

    if(this.covInfoObj.covInfo.timeAllowed!=''){
      var a =  parseInt(this.covInfoObj.covInfo.timeAllowed);
    }
    else{
      var a = 0;
    }
      
      var toUTC = new Date(this.covInfoObj.covInfo.nextdue);
      toUTC.setDate(toUTC.getDate() + a);
      if(toUTC){
        this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
      }
      

  }

  else if((this.covInfoObj.covInfo.frequency=='WEEKLY')){
    if(this.covInfoObj.covInfo.timeAllowed!=''){
      var a =  parseInt(this.covInfoObj.covInfo.timeAllowed);
    }
    else{
      var a = 0;
    }
    var b = 7 + a;
    var toUTC = new Date();
    toUTC.setDate(toUTC.getDate() + b);
    if(toUTC){
      this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
    }
  }

  else if((this.covInfoObj.covInfo.frequency=='MONTHLY')&&(this.covInfoObj.covInfo.nextdue!='')){
    if(this.covInfoObj.covInfo.timeAllowed!=''){
      var a =  parseInt(this.covInfoObj.covInfo.timeAllowed);
    }
    else{
      var a = 0;
    }
    var toUTC = new Date(this.covInfoObj.covInfo.nextdue);
    toUTC.setMonth(toUTC.getMonth() + 1);
    toUTC.setDate(toUTC.getDate() + a);
    if(toUTC){
      this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
      console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
    }
  }

  else if((this.covInfoObj.covInfo.frequency=='QUARTERLY')&&(this.statusModel.status!='')){
    var year = new Date().getFullYear();
    var newYear = new Date().getFullYear() + 1;

    if(this.covInfoObj.covInfo.timeAllowed!=''){
      var a =  parseInt(this.covInfoObj.covInfo.timeAllowed);
    }
    else{
      var a = 0;
    }

    var today = new Date();
    var month = today.getMonth() + 1;
    var cur = (Math.ceil(month / 3));
    if((cur == 4)&&(this.statusModel.status=="N")){

      var c = "31-Dec-"+year;
      var toUTC = new Date(c);
      toUTC.setDate(toUTC.getDate() + a);
      if(toUTC){
        this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
        console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
      }

    }
    else if((cur == 4)&&(this.statusModel.status=="Y")){
      var c = "31-Mar-"+newYear;
      var toUTC = new Date(c);
      toUTC.setDate(toUTC.getDate() + a);
      if(toUTC){
        this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
        console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
      }

    }
    else if((cur == 3)&&(this.statusModel.status=="N")){
      var c = "30-Sep-"+year;
      var toUTC = new Date(c);
      toUTC.setDate(toUTC.getDate() + a);
      if(toUTC){
        this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
        console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
      }

    }

    else if((cur == 3)&&(this.statusModel.status=="Y")){
      var c = "31-Dec-"+year;
      var toUTC = new Date(c);
      toUTC.setDate(toUTC.getDate() + a);
      if(toUTC){
        this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
        console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
      }

    }

    else if((cur == 2)&&(this.statusModel.status=="N")){
      var c = "30-Jun-"+year;
      var toUTC = new Date(c);
      toUTC.setDate(toUTC.getDate() + a);
      if(toUTC){
        this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
        console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
      }

    }

    else if((cur == 2)&&(this.statusModel.status=="Y")){
      var c = "30-Sep-"+year;
      var toUTC = new Date(c);
      toUTC.setDate(toUTC.getDate() + a);
      if(toUTC){
        this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
        console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
      }

    }

    else if((cur == 1)&&(this.statusModel.status=="N")){
      var c = "31-Mar-"+year;
      var toUTC = new Date(c);
      toUTC.setDate(toUTC.getDate() + a);
      if(toUTC){
        this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
        console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
      }

    }

    else if((cur == 1)&&(this.statusModel.status=="Y")){
      var c = "30-Jun-"+year;
      var toUTC = new Date(c);
      toUTC.setDate(toUTC.getDate() + a);
      if(toUTC){
        this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
        console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
      }

    }
    console.log(cur)

  }

  else if((this.covInfoObj.covInfo.frequency=='Half-Yearly')&&(this.statusModel.status!='')){
    var year = new Date().getFullYear();
    var newYear = new Date().getFullYear() + 1;

    if(this.covInfoObj.covInfo.timeAllowed!=''){
      var a =  parseInt(this.covInfoObj.covInfo.timeAllowed);
    }
    else{
      var a = 0;
    }

    var today = new Date();
    var month = today.getMonth() + 1;
    var cur = (Math.ceil(month / 6));
    if((cur == 2)&&(this.statusModel.status=="N")){

      var c = "31-Dec-"+year;
      var toUTC = new Date(c);
      toUTC.setDate(toUTC.getDate() + a);
      if(toUTC){
        this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
        console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
      }

    }

    else if((cur == 2)&&(this.statusModel.status=="Y")){

      var c = "30-Jun-"+newYear;
      var toUTC = new Date(c);
      toUTC.setDate(toUTC.getDate() + a);
      if(toUTC){
        this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
        console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
      }

    }

    else if((cur == 1)&&(this.statusModel.status=="N")){

      var c = "30-Jun-"+year;
      var toUTC = new Date(c);
      toUTC.setDate(toUTC.getDate() + a);
      if(toUTC){
        this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
        console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
      }

    }

    else if((cur == 1)&&(this.statusModel.status=="Y")){

      var c = "31-Dec-"+year;
      var toUTC = new Date(c);
      toUTC.setDate(toUTC.getDate() + a);
      if(toUTC){
        this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
        console.log(this.datePipe.transform(toUTC,"d-MMM-y"));
      }

    }

  }

  else if((this.covInfoObj.covInfo.frequency=='Yearly')){
    var year = new Date().getFullYear();

    if(this.covInfoObj.covInfo.timeAllowed!=''){
      var a =  parseInt(this.covInfoObj.covInfo.timeAllowed);
    }
    else{
      var a = 0;
    }

    var c = "31-Dec-"+year;

    var toUTC = new Date(c);
    toUTC.setDate(toUTC.getDate() + a);
    if(toUTC){
      this.expiryDate = this.datePipe.transform(toUTC,"d-MMM-y");
    }
  }


}

statusChange(value: string){
  this.getExpiryDate();
}

checkNotification():boolean{
  var resp;
  resp = false;

  if(this.rmValue!=''){
    resp =  true;
  }
  else if(this.customerValue!=''){
    resp =  true;
  }
  else if((this.rmValue!='')&&(this.customerValue!='')){
    resp =  true;
  }
  else{
    resp = false;
  }
  return resp;
}


checkFrequecy():boolean{
  var resp;
  resp = false;

  if(((this.covInfoObj.covInfo.frequency=="MONTHLY") || (this.covInfoObj.covInfo.frequency=="ONE-OFF"))&& (this.covInfoObj.covInfo.nextdue!='')){
    resp =  true;
  }
  else if((this.covInfoObj.covInfo.frequency=="QUARTERLY")&&(this.statusModel.status!='')){
    resp =  true;
  }

  else if((this.covInfoObj.covInfo.frequency=="HALF-YEARLY")&&(this.statusModel.status!='')){
    resp =  true;
  }
  else if((this.covInfoObj.covInfo.frequency=="YEARLY")||(this.covInfoObj.covInfo.frequency=="WEEKLY")){
    resp =  true;
  }
  else{
    resp = false;
  }
  return resp;
}

submitData(){
  this.loaderd = true;
  if((this.customerValue!='') && (this.rmValue!='')){
    this.covInfoObj.covInfo.notify = "CUSTOMER/RM"
  }else if((this.customerValue=='') && (this.rmValue!='')){
    this.covInfoObj.covInfo.notify = "RM";
  }else if((this.customerValue!='') && (this.rmValue=='')){
    this.covInfoObj.covInfo.notify = "CUSTOMER/RM";
  }

  if((this.covInfoObj.covInfo.frequency=="QUARTERLY")||(this.covInfoObj.covInfo.frequency=="HALF-YEARLY")|| (this.covInfoObj.covInfo.frequency=="YEARLY")||(this.covInfoObj.covInfo.frequency=="WEEKLY")){

    this.covInfoObj.covInfo.nextdue = '';

  }
  this.covInfoObj.covInfo.skipToNextQuarter = this.statusModel.status;
  this.subloading = true;
  this.covInfoObj.covInfo.status = "ACTIVE";
  var data = JSON.stringify(this.covInfoObj);
  this.api_service.update_covenant(data).subscribe(res=>{
    this.subresp = res;

    if(this.subresp.hostHeaderInfo.responseCode == "000"){	
      toast("Submitted successfully",3000);
      this.router.navigate(['/dashboard']);
      this.subloading = false;
      this.loaderd = false;
      jQuery('.confirmActivateModal').modal('close');
  
    }
    else{
      this.loaderd = false;
      this.subloading = false;
      toast(this.subresp.hostHeaderInfo.responseMessage ,3000);
    }
  
  },error=>{
    this.loaderd = false;
    this.subloading = false;
    toast("Ooops, Please try again later",3000);
  });
}









}
