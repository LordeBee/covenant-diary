import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CovenantViewComponent } from './covenant-view.component';

describe('CovenantViewComponent', () => {
  let component: CovenantViewComponent;
  let fixture: ComponentFixture<CovenantViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CovenantViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CovenantViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
