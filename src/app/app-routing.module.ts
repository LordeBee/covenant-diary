import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent} from './login/login.component'
import { DashboardComponent } from './dashboard/dashboard.component';
import { AddCovenantComponent } from './add-covenant/add-covenant.component';
import { CovenantViewComponent } from './covenant-view/covenant-view.component'
import { AuthGuard } from './auth/auth.guard';
import { AssignCovenantComponent } from './assign-covenant/assign-covenant.component'



const routes: Routes = [
  { path: "login", component: LoginComponent },
  { path: "dashboard", component: DashboardComponent ,canActivate: [AuthGuard] },
  { path: "add-covenant", component: AddCovenantComponent ,canActivate: [AuthGuard] },
  { path: "covenant-view/:uuid", component: CovenantViewComponent ,canActivate: [AuthGuard] },
  { path: "assign-covenant",component: AssignCovenantComponent,canActivate: [AuthGuard] },
  { path: "", redirectTo: "/dashboard", pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
